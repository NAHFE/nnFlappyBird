function nextGen() {
    calculateFitness();

    var leftMost = null;
    var leftMostPos = Infinity;

    for (var i = 0; i < pipes.length; i++) {
        if (pipes[i].x < leftMostPos) {
            leftMost = i;
            leftMostPos = pipes[i].x;
        }
    }

    pipes.splice(leftMost, 1);

    for (var i = 0; i < totalBirds; i++) {
        birds[i] = pickOne();
    }

    savedBirds = [];
    counter = 0;
}

function pickOne() {
    var highestFitness = null;
    var highestFitnessVaule = -Infinity;

    for (var i = 0; i < savedBirds.length; i++) {
        if (savedBirds[i].fitness > highestFitnessVaule) {
            highestFitness = i;
            highestFitnessVaule = savedBirds[i].fitness;
        }
    }

    var child = new Bird(savedBirds[highestFitness].brain);
    child.mutate();
    return child;
}

function calculateFitness() {
    var sum = 0;
    for (var bird of savedBirds) {
        sum += bird.score;
    }
    console.log(sum);
    for (var bird of savedBirds) {
        bird.fitness = bird.score / sum;
    }
}
