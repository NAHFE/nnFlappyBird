const totalBirds = 100;
var birds = [];
var savedBirds = [];
var pipes = [];
var slider;
var counter = 0;
var imgs = [];

function setup() {
    createCanvas(800, 600);

    angleMode(DEGREES);

    imgs[0] = loadImage('imgs/pipe.png');
    imgs[1] = loadImage('imgs/pipe_top.png');
    imgs[2] = 'imgs/flappyBird.gif';

    slider = createSlider(1, 100, 1);

    for (var i = 0; i < totalBirds; i++) {
        birds[i] = new Bird();
    }
}

function draw() {
    //Logics
    for (var n = 0; n < slider.value(); n++) {
        if (counter % 75 == 0) {
            pipes.push(new Pipe());
            console.log('Pipe!');
        }
        counter++;

        for (var i = 0; i < pipes.length; i++) {
            if (pipes[i].offscreen()) {
                pipes.splice(i, 1);
            }

            for (var j = 0; j < birds.length; j++) {
                if (pipes[i].hits(birds[j])) {
                    birds[j].img.remove();
                    savedBirds.push(birds.splice(j, 1)[0]);
                }
            }

            pipes[i].update();
        }

        for (var i = 0; i < birds.length; i++) {
            if (
                birds[i].y >= height - birds[i].height / 2 ||
                birds[i].y <= birds[i].height / 2
            ) {
                birds[i].img.remove();
                savedBirds.push(birds.splice(i, 1)[0]);
            }
        }

        for (var bird of birds) {
            bird.think();
            bird.update();
        }

        if (birds.length == 0) {
            pipes = [];
            nextGen();
        }
    }

    //Drawing
    background(225);

    for (var bird of birds) {
        bird.show();
    }
    for (var pipe of pipes) {
        pipe.show();
    }
}

function getNearestPipe() {
    var LeftMost = null;
    var LeftMostPos = Infinity;

    for (var i = 0; i < pipes.length; i++) {
        var distance = pipes[i].x + pipes[i].w - birds[0].x;
        if (!(distance < 0) && distance < LeftMostPos) {
            LeftMost = i;
            LeftMostPos = distance;
        }
    }

    return LeftMost;
}
