class Pipe {
    constructor() {
        this.spacing = 125;
        this.top = random(height / 6, (3 / 4) * height);
        this.bottom = this.top + this.spacing;

        this.x = width;
        this.w = 80;
        this.speed = 3;
    }

    hits(bird) {
        var hit = false;
        if (bird.x > this.x && bird.x < this.x + this.w) {
            if (bird.y < this.top || bird.y > this.bottom) {
                hit = true;
            }
        }
        return hit;
    }

    show() {
        //rect(this.x, 0, this.w, this.top);
        //rect(this.x, this.bottom, this.w, height - this.bottom);

        image(imgs[0], this.x, this.bottom, this.w, height - this.bottom);
        image(imgs[0], this.x, 0, this.w, this.top + 1);

        push();
        translate(this.x + this.w / 2, this.top + 2);
        scale(-1, 1);
        rotate(180);
        image(imgs[1], -this.w / 2 - 7, 0, this.w + 14, 50);
        pop();

        image(imgs[1], this.x - 7, this.bottom - 1, this.w + 14, 50);
    }

    update() {
        this.x -= this.speed;
    }

    offscreen() {
        return this.x < -this.w;
    }
}
