function mutate(x) {
    if (random(1) < 0.1) {
        let offset = randomGaussian() * 0.5;
        let newx = x + offset;
        return newx;
    } else {
        return x;
    }
}

class Bird {
    constructor(brain) {
        this.y = height / 2;
        this.x = 64;

        this.gravity = 0.6;
        this.lift = -10;
        this.velocity = 0;

        this.width = 40;
        this.height = 32;

        this.score = 0;
        this.fitness = 0;

        if (brain) {
            this.brain = brain.copy();
        } else {
            this.brain = new NeuralNetwork(4, 4, 2);
        }

        this.img = createImg(imgs[2]);
        this.img.size(this.width, this.height);
    }

    show() {
        push();
        stroke(255);
        fill(150, 50);
        this.img.position(this.x - this.width / 4, this.y - this.height / 4);
        //ellipse(this.x, this.y, this.height);
        //ellipse(this.x, this.y, 5, 5);
        pop();
    }

    up() {
        this.velocity = this.lift;
    }

    mutate() {
        this.brain.mutate(mutate);
    }

    think() {
        var leftMost = getNearestPipe();
        var inputs = [];
        inputs[0] = this.y / height;
        inputs[1] = pipes[leftMost].top / height;
        inputs[2] = pipes[leftMost].bottom / height;
        inputs[3] = pipes[leftMost].x / width;

        var output = this.brain.predict(inputs);
        if (output[0] < output[1]) {
            this.up();
        }
    }

    update() {
        this.score++;

        this.velocity += this.gravity;
        this.y += this.velocity;
    }
}
